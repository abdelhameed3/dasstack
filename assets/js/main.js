/*global $ */
/*eslint-env browser*/
/* Add active  class in home slider */
$(document).ready(function () {
    'use strict';
    $('.team .slider').slick({
        dots: false,
        arrows:false,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
            breakpoint: 1199,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 767,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              dots: true
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              dots: true
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });
});

  /* slider at mobile in home page */
  $(document).ready(function (){
    'use strict';

    function reslider(){
        if ($('.container').width() <= 540 ){
            $(".we-trust .slider").slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true,
                dots: true,
                arrows:false
            });
        }
    };
    reslider();
    $(window).resize(function(){     
        reslider();
    }); 
}); 
//========================Load=====================//
$(document).ready(function () {
  "use strict";
  $(window).on('load',function () {
      $(".load").fadeOut(50, function () {
          $("body").css("overflow", "auto");
          $(this).fadeOut(50, function () {
              $(this).remove();
          });
      });
  });

});
//========================Scroll To Top=====================//
$(document).ready(function (){

  var scrollButton=$("#scroll");

  $(window).scroll(function(){
      if($(this).scrollTop()>=700)
      {
          scrollButton.show();
      }
      else
      {
          scrollButton.hide();
      }
  });
  scrollButton.click(function(){
      $("html,body").animate({scrollTop:0},900);
  });
});
$(document).ready(function () {
  "use strict";
  $("nav li a").each(function () {
          var t = window.location.href.split(/[?#]/)[0];
          this.href == t && ($(this).addClass("active"));
      })
});